package com.example.adi;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class AdiRestController {
    private static final Logger logger = LoggerFactory.getLogger(AdiRestController.class);

    private final ListOperations<String, Float> ops;

    private final ExecutorService executorService;

    @Autowired
    public AdiRestController(RedisTemplate<String, Float> redisTemplate, ExecutorService executorService) {
        ops = redisTemplate.opsForList();
        this.executorService = executorService;
    }

    private CompletableFuture<List<Float>> lookup(String key) {
        return CompletableFuture.supplyAsync(() -> {
            long begin = System.currentTimeMillis();
            List<Float> l = ops.range(key, 0, -1);

            if (l == null) {
                return Collections.emptyList();
            }

            logger.info("redis took {}", (System.currentTimeMillis() - begin));
            return l;
        }, executorService);
    }

    @PostMapping
    @RequestMapping(value = "/init")
    public void initRedis() {
        for (int i = 0; i < 1000000; i++) {
            List<Float> vals = new ArrayList<>(10);
            for (int j = 0; j < 10; j++) {
                vals.add((float)i * j);
            }

            ops.rightPushAll(Integer.toString(i), vals);
        }
    }

    @PostMapping
    public List<Float> home(@RequestBody SearchReq req) {
        long begin = System.currentTimeMillis();
        CompletableFuture<List<Float>> cf1 = lookup(req.getUid());
        CompletableFuture<List<Float>> cf2 = lookup(req.getAid());

        try {
            long b1 = System.currentTimeMillis();
            List<Float> l1 = cf1.get();
            logger.info("get cf1 took {}", System.currentTimeMillis() - b1);
            b1 = System.currentTimeMillis();
            List<Float> l2 = cf2.get();
            logger.info("getting cfg2 took {}", System.currentTimeMillis() - b1);

            List<Float> ret = new ArrayList<>(l1.size() + l2.size() + req.getRt().size());

            ret.addAll(l1);
            ret.addAll(l2);
            ret.addAll(req.getRt());

            logger.info("POST got {} ms", (System.currentTimeMillis() - begin));
            return ret;
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}
