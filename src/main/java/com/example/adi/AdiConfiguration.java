package com.example.adi;

import io.lettuce.core.ReadFrom;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class AdiConfiguration {
    @Bean
    public LettuceConnectionFactory singleRedisConnectionFactory(@Value("${spring.redis.host}") String redisHostName) {
        LettuceClientConfiguration clientConfig = LettuceClientConfiguration.builder()
                .readFrom(ReadFrom.SLAVE_PREFERRED)
                .build();

        RedisStandaloneConfiguration serverConfig = new RedisStandaloneConfiguration(redisHostName, 6379);

        return new LettuceConnectionFactory(serverConfig, clientConfig);
    }

    @Bean(value = "redisTemplate")
    public RedisTemplate<String, Float> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Float> ret = new RedisTemplate<>();
        ret.setConnectionFactory(redisConnectionFactory);
        return ret;
    }

    @Bean
    public ExecutorService executorService() {
        return Executors.newFixedThreadPool(1024);
    }

    @Bean
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(512);
        executor.setMaxPoolSize(512);
        executor.setThreadNamePrefix("default_task_executor_thread");
        executor.initialize();
        return executor;
    }
}
