package com.example.adi;

import java.util.List;

public class SearchReq {
    private String uid;
    private String aid;
    private List<Float> rt;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public List<Float> getRt() {
        return rt;
    }

    public void setRt(List<Float> rt) {
        this.rt = rt;
    }
}
